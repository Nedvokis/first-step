var path = require('path');
var express = require('express');
var register = express.Router();
var User =  new require(path.join('..', 'user'));

//passport.authenticate('twitter');('facebook');('google');('linkedin'); //for future

function findRenderUsers(res){
     User.find({}, function(err, users) {
      res.render('register', { title: 'Craft your self', users: users });
      console.log(users);
    });
}

register.get('/', function(req, res, next) {
  findRenderUsers(res);
});

register.post('/', function(req, res, next) {

  var user = new User(req.body);
  console.log(req.body);
  user.save(function (err) {
    if (err) {
      return '';
    };

    findRenderUsers(res);
  });
});

module.exports = register;