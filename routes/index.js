var path = require('path');
var express = require('express');
var index = express.Router();

index.get('/',function(req, res, next){
  res.render('index', { title: 'Main page'});
});

module.exports = index;